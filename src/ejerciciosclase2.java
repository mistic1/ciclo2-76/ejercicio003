
import java.text.DecimalFormat;
import java.util.Scanner;

public class ejerciciosclase2 {
    public static void main(String[] args) throws Exception {
        // ejercicio4();
        // ejercicio5();
        ejercicio6();
    }
   
    //Pasar Km/s a m/s    
    public static void ejercicio4(){   
        System.out.println("---Pasar Km/s a m/s---");
        Scanner leernum = new Scanner(System.in);
        System.out.print("Ingrese el valor en Km/h: ");
        double velos1 = leernum.nextDouble();
        Double velos2 = velos1*1000/3600;
        DecimalFormat formato1 = new DecimalFormat("#.##");
        System.out.println(velos1+" Km/h"+" = "+(formato1.format(velos2))+" m/s");
        leernum.close();
    }
    //Calcular hipotenusa de un triangulo rectangulo
    public static void ejercicio5(){
        System.out.println("---Calcular Hipotenusa---");
        float catetoA, catetoB;
        Scanner leernum = new Scanner(System.in);
        System.out.print("Digite Cateto A: ");
        catetoA = leernum.nextFloat();
        System.out.print("Digite Cateto B: ");
        catetoB = leernum.nextFloat();
        double hipo = Math.sqrt((catetoA*catetoA)+(catetoB*catetoA));
        DecimalFormat formato1 = new DecimalFormat("#.##");
        System.out.println("La hipotenusa = "+(formato1.format(hipo)));
        leernum.close();
    }
    //Mostrar si el numero es multiplo de 10
    public static void ejercicio6(){
        System.out.println("---Multiplo de 10---");
        Scanner leernum = new Scanner(System.in); 
        System.out.print("Digite un numero entero: ");
        int num2 = 10;
        int num1 = leernum.nextInt();
        if (num1%num2==0){
            System.out.print("El numero "+num1+" es multiplo de 10");
        }
        else if (num1%num2!=0){
            System.out.println("El numero "+num1+" NO es multiplo de 10");
        }
        leernum.close();
    }
   
}
